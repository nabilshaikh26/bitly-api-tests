# Bitly API Tests

This Cypress + Cucumber based sample API test automation project is implemented with the help of JavaScript and uses the [BDD](https://cucumber.io/docs/bdd/) approach. This project aims to test the [Bitly APIs](https://dev.bitly.com/api-reference). You can visit the official [documentation](https://dev.bitly.com/) page to learn more.

## Description

#### Project Folder Structure:

```
├── cypress
│   └── /e2e/specs (feature files)
│   └── /e2e/step-definitions (tests code)
│   └── /fixtures (All test data goes here)
│   └── /screenshots (capture screenshots on failure)
│   └── /support (contains all the supporting test files)
├── cypress.config.js (cypress global configuration)

```

#### Salient Features:

- Implemented using the latest version of Cypress [(v13.6.4)](https://docs.cypress.io/guides/references/changelog#13-6-4) as of today.
- Based on Cucumber / Gherkin standard.

## Getting Started

### Installation:

** *Please read the [prerequisites](https://docs.cypress.io/guides/getting-started/installing-cypress#System-requirements) if you encounter any issues during installation.*

1. Clone the project.
2. Open the project using [Visual Studio Code](https://code.visualstudio.com/download) or any IDE of your choice.
3. Now, go to terminal and run `npm install`.<br>

### How To Run Tests?

(A) Headed Mode:

- To execute tests in browser/headed mode, run `npm run cy:open` command in terminal. This will open an interactive cypress test runner.

(B) Headless Mode:

- To execute tests in headless mode, run `npm run cy:run`.

## Contact

<a href="mailto:nabilshaikh26@gmail.com"><img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" /> &nbsp; <a href="https://www.github.com/nabilshaikh"><img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white"/> &nbsp; <a href="https://www.gitlab.com/nabilshaikh26"><img src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white"/> &nbsp; <a href="https://www.linkedin.com/in/nabil-shaikh-5362b71b3/"><img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white"/>

<right><p align="right">(<a href="#bitly-api-tests">back to top</a>)</p></right>
