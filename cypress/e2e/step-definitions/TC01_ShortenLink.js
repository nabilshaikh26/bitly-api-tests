/* eslint-disable linebreak-style */
/* eslint-disable import/no-extraneous-dependencies */
import { Given, When } from '@badeball/cypress-cucumber-preprocessor';

let baseURL;
let shortenedLink;
let inputURL;
let bitlink;
let getBitlink;

Given('the base url {string}', (url) => {
  baseURL = url;
});

// Scenario-1

When('I make the {string} request to shortened the given URL {string} using endpoint {string}', (httpMethod, url, endpoint) => {
  inputURL = url;
  cy
    .request({
      method: httpMethod,
      url: baseURL + endpoint,
      headers: {
        Authorization: `Bearer ${Cypress.env('USER_TOKEN')}`,
        'Content-Type': 'application/json',
      },
      body: {
        long_url: inputURL,
        domain: 'bit.ly',
      },
    })
    .then((response) => {
      expect(response.status).to.equal(200);
      expect(response.body).to.have.property('long_url', inputURL);
      shortenedLink = response.body.link;
    });
});

When('the new shortened link should be created successfully and verify the same using another endpoint {string}', (endpoint) => {
  bitlink = shortenedLink.replace(/^https:\/\//, '');
  cy
    .request({
      method: 'GET',
      url: baseURL + endpoint + bitlink,
      headers: {
        Authorization: `Bearer ${Cypress.env('USER_TOKEN')}`,
        'Content-Type': 'application/json',
      },
    })
    .then((response) => {
      expect(response.status).to.equal(200);
      expect(response.body.long_url).to.equal(inputURL);
    });
  cy
    .visit(shortenedLink, { timeout: 20000 })
    .then((resp) => {
      cy
        .url()
        .should('equal', inputURL);
    });
});

When('verify the shortened link to be redirected to {string}', (url) => {
  cy
    .visit(shortenedLink, { timeout: 20000 });
  cy
    .url()
    .should('equal', url);
});

// Scenario-2

When('I make a {string} request to create a new bitlink for a URL {string} using endpoint {string}', (httpMethod, url, endpoint) => {
  inputURL = url;
  cy
    .request({
      method: httpMethod,
      url: baseURL + endpoint,
      headers: {
        Authorization: `Bearer ${Cypress.env('USER_TOKEN')}`,
        'Content-Type': 'application/json',
      },
      body: {
        long_url: inputURL,
        domain: 'bit.ly',
      },
    })
    .then((response) => {
      expect(response.status).to.equal(200);
      expect(response.body).to.have.property('long_url', inputURL);
      bitlink = response.body.link;
    });
});

When('I update the bitlink title to {string} by making a {string} request to endpoint {string}', (title, httpMethod, endpoint) => {
  cy
    .request({
      method: httpMethod,
      url: baseURL + endpoint + bitlink.replace(/^https:\/\//, ''),
      headers: {
        Authorization: `Bearer ${Cypress.env('USER_TOKEN')}`,
        'Content-Type': 'application/json',
      },
      body: {
        title,
        domain: 'bit.ly',
      },
    })
    .then((response) => {
      getBitlink = response.body.link;
      expect(response.status).to.equal(200);
      expect(response.body).to.have.property('title', title);
    });
});

When('the bitlink should still be redirected to {string} and should not be broken', (url) => {
  cy
    .visit(getBitlink, { timeout: 20000 });
  cy
    .url()
    .should('equal', url);
});
