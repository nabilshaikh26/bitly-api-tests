Feature: Shorten a Link

As a tester, I want to create/update the shortened link using the API and verify the corresponding response/result.

    Background: Initialize the base url
        * the base url 'https://api-ssl.bitly.com'

    Scenario: Shorten a link using POST method
        When I make the 'POST' request to shortened the given URL 'https://www.medscapelive.com/events?specialty=dermatology' using endpoint '/v4/shorten'
        Then the new shortened link should be created successfully and verify the same using another endpoint '/v4/bitlinks/'
        And verify the shortened link to be redirected to 'https://www.medscapelive.com/events?specialty=dermatology'
    
    Scenario: Update the bitlink using PATCH method
        Given I make a 'POST' request to create a new bitlink for a URL 'https://www.medscapelive.com/events?specialty=dermatology' using endpoint '/v4/shorten'
        When I update the bitlink title to 'MedscapeLIVE' by making a 'PATCH' request to endpoint '/v4/bitlinks/'
        Then the bitlink should still be redirected to 'https://www.medscapelive.com/events?specialty=dermatology' and should not be broken